# KATSOA: Keyless AuTomation of Sight ObservAtion
KATSOA is an application that detects fatigue based on eye data collected from glasses.

**This is a prototype. It is public to be a part of a personal resumé. Do not use it in productive use without carefully reviewing and adjusting the code.**

It was developed during the hackathon [Junction 2023](www.junction2023.com) as a solution for the [challenge](https://www.pixieray.com/) proposed by [Pixieray Oy](https://www.pixieray.com/). 

The team consisted of (ordered alphabetically in respect to the family names):
- [Jakob Lerch](https://www.gitlab.com/jlerch)
- Nico Renaldo
- Qiong Wu
- Dongyiu Wu
- Chen Xu

