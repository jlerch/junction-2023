import socketio

# Initialize SocketIO instance
sio = socketio.Client()

# Event handler for connection established


@sio.event
def connect():
    print('Connected to server')

# Event handler for a custom event 'stream_event'


@sio.event
def data(data):
    print('Received event:', data)


if __name__ == "__main__":
    # Connect to the SocketIO server
    sio.connect('http://localhost:5000')

    # Keep the program running to receive messages
    sio.wait()
