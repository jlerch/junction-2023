import json
import time
from threading import Event, Thread

import numpy as np
from flask import Flask, jsonify
from flask_cors import CORS
from flask_socketio import SocketIO

from katsoa import data
from katsoa.data import calc_fatigue, calc_fatigue_ma, get_data_imu

flask_app = Flask(__name__)
cors = CORS(flask_app)
socketio = SocketIO(flask_app, cors_allowed_origins="*")

stop_event = Event()

is_streaming = True


def stream_data():
    global is_streaming

    df = get_data_imu()
    for index, row in df.iterrows():
        if not is_streaming:
            return

        # Normal posture (Looking forward) has the value at around 80 to 100 degree
        # Other than that, bad posture!
        label = "" if (
            row['roll'] > 80 and row['roll'] < 100) else "Bad Posture!"
        data = {
            "pitch": row['roll'],
            "label": label
        }

        # Sending data at the average interval of the sensor data (16 ms)
        time.sleep(16 / 1000)
        socketio.emit('data', json.dumps(data))

    is_streaming = False


@flask_app.route('/')
def home():
    message = {'message': 'Hello Katsoa', "version": "1.0.0"}
    return jsonify(message), 200


@flask_app.route('/home')
def home_api():
    message = {
        "data": {
            "fatigue_level": 20,
            "work_hour": 5,
            "sleep_hour": 8,
            "productivity": 90
        },
    }
    return jsonify(message), 200


@flask_app.route('/focus')
def focus_history():
    message = {
        "data": [np.random.randint(0, 100) for _ in range(50)],
    }
    return jsonify(message), 200


@flask_app.route('/fatigue')
def fatigue():
    t, d = calc_fatigue()
    message = {
        "time": t.tolist(),
        "fatigue_level": d.tolist()
    }
    return jsonify(message), 200


@flask_app.route('/fatigue-ma')
def fatigue_ma():
    fatigues_ma = calc_fatigue_ma()

    message = {
        "fatigue_level": fatigues_ma
    }
    return jsonify(message), 200


@flask_app.route('/start-stream', methods=["POST"])
def start_stream():
    global is_streaming
    if is_streaming:
        return jsonify({'status': 'Already streaming'}), 400

    is_streaming = True
    stream_thread = Thread(target=stream_data)
    stream_thread.start()
    return jsonify({'status': 'Streaming started'}), 200


@flask_app.route('/stop-stream', methods=["POST"])
def stop_stream():
    global is_streaming
    is_streaming = False
    return jsonify({'status': 'Streaming stopped'}), 200
