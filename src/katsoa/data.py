import numpy as np
import pandas as pd
import scipy as sp

from katsoa.constants import SAMPLE_INTERVAL


def get_data_imu():
    file_path = "data-investigation/data/Indoor/Participant_1_IMU.csv"
    df = pd.read_csv(file_path)
    df['roll'] = np.degrees(np.arctan2(
        df['Y'], np.sqrt(df['X']**2 + df['Z']**2)))
    df = df[(df['ticktime_aligned'] >= 0) & (df['ticktime_aligned'] <= 120000)]
    return df


def get_data_afe():
    file_path = "data-investigation/data/Indoor/Participant_1_AFE.csv"
    df = pd.read_csv(file_path)
    return df


def calc_fatigue():
    df = get_data_afe()

    time = df["ticktime_aligned"].to_numpy()

    ir = df.loc[:, "left_eye_0":"left_eye_2"]

    # calculate differentiation
    ir_filtered = ir.apply(lambda x: sp.signal.savgol_filter(x, 10, 2))
    ir_diff = ir_filtered.apply(np.diff)
    ir_diff.loc[-1] = np.zeros(ir_diff.shape[1])  # adding a row
    ir_diff.index = ir_diff.index + 1  # shifting index
    ir_diff = ir_diff.sort_index()

    # detect blink status
    blink_per_sensor = ir_diff > 300
    blink_overall = np.all(blink_per_sensor, axis=1).to_numpy()

    # get signal of false-true transitions
    blink_false_true = np.roll(blink_overall, -1) & blink_overall

    # calculate moving average
    w = 60000//SAMPLE_INTERVAL  # window of one minute
    blink_moving_avg = np.convolve(blink_overall, np.ones(w), 'valid') / w

    # convert moving average to fatigue score
    # ..calculate threshold by which a person is said to be fatigue
    threshold = 4 / (6000 / SAMPLE_INTERVAL)
    # ..convert moving average
    fatigue_score = -np.exp(-4*(blink_moving_avg - threshold)) + 1

    return (time, fatigue_score)


def calc_fatigue_ma():
    _, fatigues = calc_fatigue()
    window_size = 12
    fatigues_ma = np.convolve(fatigues, np.ones(
        window_size) / window_size, mode='valid')

    if len(fatigues) >= window_size:
        fatigues_ma = [sum(fatigues[i:i+window_size]) /
                       window_size for i in range(len(fatigues) - window_size + 1)]
        return fatigues_ma
    else:
        print("Insufficient data for the window size.")
        return []
