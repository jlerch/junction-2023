from typer import Typer

from katsoa.api import flask_app, socketio

app = Typer()


@app.command()
def flask():
    socketio.run(flask_app)


if __name__ == "__main__":
    app()
